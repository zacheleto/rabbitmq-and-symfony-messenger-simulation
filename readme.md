# RabbitMQ and Symfony messenger simulation

* A simulation that includes the basics of RabbitMQ message broker with CQRS pattern - command query responsibility segregation - implemented thanks to the symfony Messenger Component where Commands are responsible for writing queries to the database but Queries responsible for select queries to the database and instead of writing CQRS architecture, Messenger component allow us in this project to use what's called Messages which will be sent to RabbitMQ server to be stored there then sent into other microservices where Messenger Component is installed to recieve and handle the commands and queries that come from the first app, in this project that's the e-shop website app.
* In this project simulation The RabbitMQ comes with two exchanges (Fanout and Direct exchange) and the E-shop website which is an ecommerce system, this application is going to produce three messages (show search results QUERY - singup to SNS Newsletter COMMAND - order shopping cart products COMMAND) all of those are messages ONE query and TWO commands. Sign up to SMS Newsletter is going to be sent to direct exchange and the direct exchange will resend this to one of those SMS service apps as you can see in the image bellow and the message will be sent there randomly to one of those two which is common practice if we want to scale horizontally our application, if the application is exposed to high traffic then such solution comes very handy and of course this is only a simulation we can have there many microservices not only two and such solution is similar to so called Round-robin DNS.
* The Order data will be sent to the Fanout exchange unlike the direct exchange the Fanout will send the message to all of existing application connected with the fanout exchange not only those two microservices as shown in the Direct exchange, the first application on this site is email service application because after a user orders shopping cart products an email will be sent to him to confirm the process and at the same time the Warehouse system will updated because physical stores need to know about the updated stock so the user will be informed wether that product still exist or is out-of stock.
![](simulation.jpg)


## Built With

* [Symfony]
* [Symfony Messenger]
* [RabbitMQ]


## Author

* **Zache Abdelatif (Leto)** 
